console.log("Завдання 3");

const user1 = {
    name1: "John",
    years: 30
};

const {name1: nameUser, years: yearsUser, isAdmin = false} = user1;
console.log(`${nameUser}, ${yearsUser}, ${isAdmin}`);